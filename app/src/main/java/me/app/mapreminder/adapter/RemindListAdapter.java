package me.app.mapreminder.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.arcgisruntime.location.LocationDataSource;

import java.util.ArrayList;

import me.app.mapreminder.MainActivity;
import me.app.mapreminder.R;
import me.app.mapreminder.activity.ReminderSettingActivity;
import me.app.mapreminder.database.Database;
import me.app.mapreminder.model.Reminder;
import me.app.mapreminder.service.ReminderService;

/**
 * Created by Adisorn on 10/12/2559.
 */

public class RemindListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Reminder> mDataList;
    private Database db;

    public RemindListAdapter(Context context, ArrayList<Reminder> list) {
        this.context = context;
        this.mDataList = list;
        db = new Database(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.remind_list_row, parent, false);
        RecyclerView.ViewHolder vh = new ReminderViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ReminderViewHolder) holder).title.setText(mDataList.get(position).getName());
        if(mDataList.get(position).getStatus() == Reminder.ENABLE) {
            ((ReminderViewHolder) holder).checkBox.setImageResource(R.drawable.ic_check_circle_black_24dp);
        }
        else {
            ((ReminderViewHolder) holder).checkBox.setImageResource(R.drawable.ic_radio_button_unchecked_black_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    private class ReminderViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView checkBox;

        public ReminderViewHolder(View v) {
            super(v);

            title = (TextView) v.findViewById(R.id.title);
            checkBox = (ImageView) v.findViewById(R.id.state);

            v.setBackgroundResource(R.drawable.list_background);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ReminderSettingActivity.class);
                    intent.putExtra("alarm_id", mDataList.get(getAdapterPosition()).getId());
                    ((Activity) context).startActivityForResult(intent, 1);
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    new AlertDialog.Builder(context).setTitle("DELETE!")
                            .setMessage("Are you sure?")
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    db.deleteAlarm(mDataList.get(getAdapterPosition()).getId());
                                    mDataList.remove(getAdapterPosition());
                                    notifyDataSetChanged();
                                    context.startService(new Intent(context, ReminderService.class));
                                }
                            })
                            .show();
                    return true;
                }
            });

            final int color = Color.parseColor("#1E88E5");

            checkBox.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mDataList.get(getAdapterPosition()).getStatus() == Reminder.ENABLE) {
                        mDataList.get(getAdapterPosition()).setStatus(Reminder.DISABLE);
                        checkBox.setImageResource(R.drawable.ic_radio_button_unchecked_black_24dp);
                        showMessage("DISABLE");

                        checkBox.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                    }
                    else {
                        mDataList.get(getAdapterPosition()).setStatus(Reminder.ENABLE);
                        checkBox.setImageResource(R.drawable.ic_check_circle_black_24dp);
                        checkBox.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                        showMessage("ENABLE");
                    }
                    db.setAlarmState(mDataList.get(getAdapterPosition()).getId(), mDataList.get(getAdapterPosition()).getStatus());
                    context.startService(new Intent(context, ReminderService.class));
                }
            });

        }
    }

    public void clear() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<Reminder> list) {
        mDataList = list;
        notifyDataSetChanged();
    }

    private void logDisplay(String log) {
        Log.d("Debug", log);
    }

    private void showMessage(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
