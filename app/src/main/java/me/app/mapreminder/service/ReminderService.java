package me.app.mapreminder.service;


import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;

import me.app.mapreminder.MainActivity;
import me.app.mapreminder.R;
import me.app.mapreminder.activity.AlertActivity;
import me.app.mapreminder.database.Database;
import me.app.mapreminder.model.Reminder;

/**
 * Created by Adisorn on 10/12/2559.
 */

public class ReminderService extends Service {

    private Location location;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private double latitude;
    private double longitude;
    private ArrayList<Reminder> reminders;
    private Database db;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("service", "service destroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("onStart", "onStartCommand first line");
        db = new Database(getApplicationContext());
        reminders = db.getEnableAlarms();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                //Log.d("latitude", Double.toString(latitude));
                //Log.d("longitude", Double.toString(longitude));

                latitude = location.getLatitude();
                longitude = location.getLongitude();

                boolean findPlace = false;
                //Log.d("total", Integer.toString(reminders.size()));

                for(Reminder reminder: reminders) {
                    Location current = new Location("");
                    current.setLatitude(latitude);
                    current.setLongitude(longitude);

                    Location reminderLocation = new Location("");
                    reminderLocation.setLatitude(reminder.getLatitude());
                    reminderLocation.setLongitude(reminder.getLongitude());
                    Log.d("distance", Double.toString(current.distanceTo(reminderLocation)) + " " + reminder.getName());
                    if(current.distanceTo(reminderLocation) <= reminder.getDistance()) {
                        Log.d("Near Location", reminder.getName());
                        findPlace = true;
                        db.setAlarmState(reminder.getId(), Reminder.DISABLE);
                        Notification notification = null;

                        Intent notic = new Intent(getApplication(), AlertActivity.class);
                        notic.putExtra("alarm_id", reminder.getId());
                        notic.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        PendingIntent pending = PendingIntent.getActivity(getApplicationContext(),
                                0, notic, PendingIntent.FLAG_UPDATE_CURRENT);

                        if(reminder.getType() == Reminder.VIBRATE) {
                            notification =
                                    new NotificationCompat.Builder(getApplicationContext()) // this is context
                                            .setSmallIcon(R.drawable.ic_alarm_white_48dp)
                                            .setContentTitle(reminder.getName())
                                            .setContentText("Reach your goal")
                                            .setAutoCancel(true)
                                            .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                                            .setContentIntent(pending)
                                            .build();
                        }
                        else {
                            notification =
                                    new NotificationCompat.Builder(getApplicationContext()) // this is context
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setContentTitle(reminder.getName())
                                            .setContentText("Reach your goal")
                                            .setAutoCancel(true)
                                            .setContentIntent(pending)
                                            .build();
                        }
                        NotificationManager notificationManager =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(1000, notification);
                    }

                    if(findPlace) {
                        reminders = db.getEnableAlarms();
                    }

                }

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return START_STICKY;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        //This service is running in the background all the time. Therefore, return sticky.
        return START_STICKY;
    }

}

