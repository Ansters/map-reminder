package me.app.mapreminder;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.logging.LogRecord;

import me.app.mapreminder.activity.ReminderSettingActivity;
import me.app.mapreminder.adapter.RemindListAdapter;
import me.app.mapreminder.database.Database;
import me.app.mapreminder.model.Reminder;
import me.app.mapreminder.service.ReminderService;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Toolbar toolbar;
    private RecyclerView alarmLists;
    private FloatingActionButton fab;
    private LinearLayoutManager layoutManager;
    private RemindListAdapter adapter;
    private ArrayList<Reminder> mDataList;
    private Database db;
    private final int LOCATION_REQUEST_CODE = 2;
    private SwipeRefreshLayout refreshLayout;
    protected Handler handler;

    protected GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildGoogleApiClient();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST_CODE);
            return;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = new Database(this);

        alarmLists = (RecyclerView) findViewById(R.id.alarm_list);
        layoutManager = new LinearLayoutManager(this);
        fab = (FloatingActionButton) findViewById(R.id.fabBtn);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.container);
        handler = new Handler();

        prepareData();
        adapter = new RemindListAdapter(this, mDataList);
        alarmLists.setAdapter(adapter);
        alarmLists.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(alarmLists.getContext(),
                layoutManager.getOrientation());
        alarmLists.addItemDecoration(dividerItemDecoration);
        initOnclick();
        startService(new Intent(this, ReminderService.class));
        if(isMyServiceRunning(ReminderService.class)) {
            logDisplay("Still running");
        }

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        prepareData();
                        adapter.clear();
                        adapter.addAll(mDataList);
                        refreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });

    }

    private void initOnclick() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), ReminderSettingActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    private void prepareData() {
        mDataList = db.getAlarms();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Intent refresh = new Intent(this, MainActivity.class);
            startActivity(refresh);
            this.finish();
        }
    }

    private void logDisplay(String log) {
        Log.d("Display", log);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    public void onConnected(Bundle connectionHint) {
        Log.i("Debug", "Connected to GoogleApiClient");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("Debug", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason.
        Log.i("Debug", "Connection suspended");

        // onConnected() will be called again automatically when the service reconnects
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
