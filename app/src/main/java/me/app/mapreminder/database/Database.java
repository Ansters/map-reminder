package me.app.mapreminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import me.app.mapreminder.model.Reminder;

/**
 * Created by Adisorn on 10/12/2559.
 */

public class Database extends SQLiteOpenHelper {

    public static final String DB_NAME = "location_reminder";
    public static final String TABLE_NAME = "reminder";
    public static final String COL_ID = "rem_id";
    public static final String COL_LAT = "rem_lat";
    public static final String COL_LONG = "rem_long";
    public static final String COL_NAME = "rem_name";
    public static final String COL_STATE = "rem_state";
    public static final String COL_ALARM_TYPE = "rem_type";
    public static final String COL_DISTANCE = "rem_distance";

    private SQLiteDatabase db;

    public Database(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_NAME + " TEXT, " +
                    COL_LAT + " DOUBLE, " +
                    COL_LONG + " DOUBLE, " +
                    COL_DISTANCE + " INTEGER, " +
                    COL_ALARM_TYPE + " INTEGER, " +
                    COL_STATE + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void addAlarm(Reminder reminder) {
        db = this.getWritableDatabase();
        ContentValues conv = new ContentValues();
        conv.put(COL_LAT, reminder.getLatitude());
        conv.put(COL_LONG, reminder.getLongitude());
        conv.put(COL_NAME, reminder.getName());
        conv.put(COL_ALARM_TYPE, reminder.getType());
        conv.put(COL_STATE, reminder.getStatus());
        conv.put(COL_DISTANCE, reminder.getDistance());
        Log.d("log", Integer.toString(reminder.getDistance()));
        db.insert(TABLE_NAME, null, conv);
        db.close();
    }

    public ArrayList<Reminder> getAlarms() {
        ArrayList<Reminder> list = new ArrayList<>();
        db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + Database.TABLE_NAME + ";", null);
        c.moveToFirst();
        while(!c.isAfterLast()) {
            Reminder reminder = new Reminder();
            reminder.setId(c.getInt(c.getColumnIndex(Database.COL_ID)));
            reminder.setName(c.getString(c.getColumnIndex(Database.COL_NAME)));
            reminder.setLatitude(c.getDouble(c.getColumnIndex(Database.COL_LAT)));
            reminder.setLongitude(c.getDouble(c.getColumnIndex(Database.COL_LONG)));
            reminder.setStatus(c.getInt(c.getColumnIndex(COL_STATE)));
            reminder.setType(c.getInt(c.getColumnIndex(Database.COL_ALARM_TYPE)));
            reminder.setDistance(c.getInt(c.getColumnIndex(COL_DISTANCE)));
            list.add(reminder);
            c.moveToNext();
        }
        db.close();
        c.close();
        return list;
    }

    public void deleteAlarm(int id) {
        db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COL_ID+"=?", new String[] { Integer.toString(id) });
        db.close();
    }

    public void setAlarmState(int id, int state) {
        db = this.getWritableDatabase();
        ContentValues conv = new ContentValues();
        conv.put(COL_STATE, state);
        db.update(TABLE_NAME, conv, COL_ID+"=?", new String[] { Integer.toString(id) });
        db.close();
    }

    public void updateAlarm(Reminder reminder) {
        db = this.getWritableDatabase();
        ContentValues conv = new ContentValues();
        conv.put(COL_LAT, reminder.getLatitude());
        conv.put(COL_LONG, reminder.getLongitude());
        conv.put(COL_NAME, reminder.getName());
        conv.put(COL_ALARM_TYPE, reminder.getType());
        conv.put(COL_STATE, reminder.getStatus());
        conv.put(COL_DISTANCE, reminder.getDistance());
        db.update(TABLE_NAME, conv, COL_ID+"=?", new String[] { Integer.toString(reminder.getId()) });
        db.close();
    }

    public Reminder getAlarm(int id) {
        Reminder reminder = new Reminder();
        db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + TABLE_NAME + " where " + COL_ID + "=" + id + ";", null);
        c.moveToFirst();
        while(!c.isAfterLast()) {
            reminder.setId(c.getInt(c.getColumnIndex(Database.COL_ID)));
            reminder.setName(c.getString(c.getColumnIndex(Database.COL_NAME)));
            reminder.setLatitude(c.getDouble(c.getColumnIndex(Database.COL_LAT)));
            reminder.setLongitude(c.getDouble(c.getColumnIndex(Database.COL_LONG)));
            reminder.setStatus(c.getInt(c.getColumnIndex(COL_STATE)));
            reminder.setType(c.getInt(c.getColumnIndex(Database.COL_ALARM_TYPE)));
            reminder.setDistance(c.getInt(c.getColumnIndex(COL_DISTANCE)));
            c.moveToNext();
        }
        db.close();
        c.close();
        return reminder;
    }

    public ArrayList<Reminder> getEnableAlarms() {
        ArrayList<Reminder> list = new ArrayList<>();
        db = this.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + Database.TABLE_NAME + " where " + COL_STATE + "=" + Reminder.ENABLE + ";", null);
        c.moveToFirst();
        while(!c.isAfterLast()) {
            Reminder reminder = new Reminder();
            reminder.setId(c.getInt(c.getColumnIndex(Database.COL_ID)));
            reminder.setName(c.getString(c.getColumnIndex(Database.COL_NAME)));
            reminder.setLatitude(c.getDouble(c.getColumnIndex(Database.COL_LAT)));
            reminder.setLongitude(c.getDouble(c.getColumnIndex(Database.COL_LONG)));
            reminder.setStatus(c.getInt(c.getColumnIndex(COL_STATE)));
            reminder.setType(c.getInt(c.getColumnIndex(Database.COL_ALARM_TYPE)));
            reminder.setDistance(c.getInt(c.getColumnIndex(COL_DISTANCE)));
            list.add(reminder);
            c.moveToNext();
        }
        db.close();
        c.close();
        return list;
    }

}
