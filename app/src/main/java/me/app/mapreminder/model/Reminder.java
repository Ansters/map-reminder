package me.app.mapreminder.model;

/**
 * Created by Adisorn on 10/12/2559.
 */

public class Reminder {

    private int id;
    private String name;
    private Double latitude;
    private Double longitude;
    private int status;
    private int type;
    private int distance;

    public static final int ENABLE = 1;
    public static final int DISABLE = 0;

    public static final int SILENCE = 2;
    public static final int VIBRATE = 3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

}
