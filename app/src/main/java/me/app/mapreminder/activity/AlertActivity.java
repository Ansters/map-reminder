package me.app.mapreminder.activity;

import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.LicenseLevel;
import com.esri.arcgisruntime.LicenseResult;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;

import me.app.mapreminder.R;
import me.app.mapreminder.database.Database;
import me.app.mapreminder.model.Reminder;

public class AlertActivity extends AppCompatActivity {

    private BottomSheetBehavior bottomSheetBehavior;
    private TextView name;
    private TextView location;
    private FloatingActionButton fab;

    private MapView mapView;
    private ArcGISMap map;
    private GraphicsOverlay graphicsOverlay;
    private LocationDisplay locationDisplay;
    private Graphic graphic;

    private int id;
    private Reminder reminder;
    private Database db;

    private static final String CLIENT_ID = "runtimelite,1000,rud7195543677,none,XXMFA0PL4S83XNX8A118";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);

        LicenseResult licenseResult = ArcGISRuntimeEnvironment.setLicense(CLIENT_ID);;
        LicenseLevel licenseLevel = ArcGISRuntimeEnvironment.getLicense().getLicenseLevel();

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        name = (TextView) findViewById(R.id.name);
        location = (TextView) findViewById(R.id.location);
        fab = (FloatingActionButton) findViewById(R.id.fabBtn);
        db = new Database(getApplicationContext());
        id = getIntent().getIntExtra("alarm_id", -1);
        reminder = db.getAlarm(id);
        SpatialReference spacRef = SpatialReference.create(4326);
        final Point point = new Point(reminder.getLongitude(), reminder.getLatitude(), spacRef);

        mapView = (MapView) findViewById(R.id.mapView);
        map = new ArcGISMap(Basemap.createStreets());
        mapView.setMap(map);
        locationDisplay = mapView.getLocationDisplay();
        locationDisplay = mapView.getLocationDisplay();
        locationDisplay.addDataSourceStatusChangedListener(new LocationDisplay.DataSourceStatusChangedListener() {
            @Override
            public void onStatusChanged(LocationDisplay.DataSourceStatusChangedEvent dataSourceStatusChangedEvent) {
                if (dataSourceStatusChangedEvent.getSource().getLocationDataSource().getError() == null) {
                    //showMessage("Location Display Started=" + dataSourceStatusChangedEvent.isStarted());
                } else {
                    //showMessage("Something went wrong.");
                }
            }
        });
        locationDisplay.startAsync();
        locationDisplay.setShowLocation(true);
        locationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);

        locationDisplay.addLocationChangedListener(new LocationDisplay.LocationChangedListener() {
            @Override
            public void onLocationChanged(LocationDisplay.LocationChangedEvent locationChangedEvent) {
                location.setText(determineLocation(point, locationChangedEvent.getLocation().getPosition()));
            }
        });

        graphicsOverlay = new GraphicsOverlay();
        mapView.getGraphicsOverlays().add(graphicsOverlay);

        BitmapDrawable pin = (BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.ic_location_on_black_36dp);
        final PictureMarkerSymbol symbol = new PictureMarkerSymbol(pin);
        symbol.setWidth(40);
        symbol.setHeight(40);
        symbol.setOffsetY(20);
        symbol.addDoneLoadingListener(new Runnable() {
            @Override
            public void run() {
                graphic = new Graphic(point, symbol);
                graphicsOverlay.getGraphics().add(graphic);
            }
        });
        symbol.loadAsync();

        name.setText(reminder.getName());
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, null);
                finish();
            }
        });

    }

    private String determineLocation(Point p1, Point p2) {
        Location location1 = new Location("");
        Location location2 = new Location("");
        location1.setLatitude(p1.getY());
        location1.setLongitude(p1.getX());
        location2.setLatitude(p2.getY());
        location2.setLongitude(p2.getX());
        double distance = location1.distanceTo(location2);
        if(distance / 1000 < 1) {
            return String.format("%.2f", distance) + " เมตร";
        }
        else {
            return String.format("%.2f", distance / 1000) + " กิโลเมตร";
        }
    }
}
