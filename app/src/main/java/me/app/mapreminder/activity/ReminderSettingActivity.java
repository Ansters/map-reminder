package me.app.mapreminder.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.LicenseLevel;
import com.esri.arcgisruntime.LicenseResult;
import com.esri.arcgisruntime.geometry.GeometryEngine;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.location.LocationDataSource;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.tasks.networkanalysis.Route;
import com.esri.arcgisruntime.tasks.networkanalysis.RouteParameters;
import com.esri.arcgisruntime.tasks.networkanalysis.RouteTask;

import me.app.mapreminder.R;
import me.app.mapreminder.database.Database;
import me.app.mapreminder.model.Reminder;
import me.app.mapreminder.service.ReminderService;

public class ReminderSettingActivity extends AppCompatActivity {

    private MapView mapView;
    private ArcGISMap map;
    private LocationDisplay locationDisplay;
    private Callout callout;
    private RouteTask mRouteTask;
    private RouteParameters mRouteParams;
    private Point mSourcePoint;
    private Point mDestinationPoint;
    private Route mRoute;
    private SimpleLineSymbol mRouteSymbol;
    private GraphicsOverlay mGraphicsOverlay;

    private static final String CLIENT_ID = "runtimelite,1000,rud7195543677,none,XXMFA0PL4S83XNX8A118";
    private final int LOCATION_REQUEST_CODE = 2;

    private ImageView locationMark;
    private LinearLayout name;
    private LinearLayout type;
    private LinearLayout distance;
    private TextView disName;
    private TextView disType;
    private TextView disDistance;
    private TextView location;
    private Handler handler;

    private BottomSheetBehavior bottomSheetBehavior;
    private FloatingActionButton fab;

    private Reminder reminder;
    private Database db;

    private int id;
    private int current_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_setting);

        reminder = new Reminder();
        reminder.setDistance(100);
        db = new Database(this);

        id = getIntent().getIntExtra("alarm_id", -1);

        LicenseResult licenseResult = ArcGISRuntimeEnvironment.setLicense(CLIENT_ID);
        LicenseLevel licenseLevel = ArcGISRuntimeEnvironment.getLicense().getLicenseLevel();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST_CODE);
            return;
        }

        fab = (FloatingActionButton) findViewById(R.id.fabBtn);
        name = (LinearLayout) findViewById(R.id.name);
        type = (LinearLayout) findViewById(R.id.alarm_type);
        distance = (LinearLayout) findViewById(R.id.distance);
        location = (TextView) findViewById(R.id.location);
        disName = (TextView) findViewById(R.id.disName);
        disType = (TextView) findViewById(R.id.disType);
        disDistance = (TextView) findViewById(R.id.disDistance);

        mapView = (MapView) findViewById(R.id.mapView);
        callout = mapView.getCallout();
        locationMark = new ImageView(this);
        locationMark.setImageResource(R.drawable.ic_location_on_black_36dp);
        locationMark.setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);

        if(id != -1) {
            reminder = db.getAlarm(id);
            map = new ArcGISMap(Basemap.Type.STREETS, reminder.getLatitude(), reminder.getLongitude(), 16);
            SpatialReference spacRef = SpatialReference.create(4326);
            final Point point = new Point(reminder.getLongitude(), reminder.getLatitude(), spacRef);
            callout.show(locationMark, point);
            disName.setText(reminder.getName());
            disDistance.setText(Integer.toString(reminder.getDistance()));
            if(reminder.getType() == Reminder.SILENCE) {
                disType.setText("Silence");
                current_type = Reminder.SILENCE;
            }
            else {
                disType.setText("Vibrate");
                current_type = Reminder.VIBRATE;
            }

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    reminder.setName(disName.getText().toString());
                    reminder.setStatus(Reminder.ENABLE);
                    reminder.setId(id);
                    if(reminder.getLatitude() != 0 && reminder.getLongitude() != 0) {
                        db.updateAlarm(reminder);
                        showMessage("Update Alarm");
                        startService(new Intent(getApplication(), ReminderService.class));
                        setResult(RESULT_OK, null);
                        finish();
                    }
                    else {
                        showMessage("Select location");
                    }

                }
            });

            handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    locationDisplay = mapView.getLocationDisplay();
                    locationDisplay.addDataSourceStatusChangedListener(new LocationDisplay.DataSourceStatusChangedListener() {
                        @Override
                        public void onStatusChanged(LocationDisplay.DataSourceStatusChangedEvent dataSourceStatusChangedEvent) {
                            if (dataSourceStatusChangedEvent.getSource().getLocationDataSource().getError() == null) {
                                //showMessage("Location Display Started=" + dataSourceStatusChangedEvent.isStarted());
                            } else {
                                //showMessage("Something went wrong.");
                            }
                        }
                    });
                    locationDisplay.startAsync();
                    locationDisplay.setShowLocation(true);
                    location.setText(determineLocation(point, locationDisplay.getLocation().getPosition()));
                    locationDisplay.addLocationChangedListener(new LocationDisplay.LocationChangedListener() {
                        @Override
                        public void onLocationChanged(LocationDisplay.LocationChangedEvent locationChangedEvent) {
                            SpatialReference spacRef = SpatialReference.create(4326);
                            if(reminder.getLatitude() != null && reminder.getLongitude() != null) {
                                final Point point = new Point(reminder.getLongitude(), reminder.getLatitude(), spacRef);
                                location.setText(determineLocation(point, locationChangedEvent.getLocation().getPosition()));
                            }

                        }
                    });
                }
            }, 5000);
        }
        else {
            current_type = Reminder.VIBRATE;
            map = new ArcGISMap(Basemap.createStreets());
            locationDisplay = mapView.getLocationDisplay();
            locationDisplay.addDataSourceStatusChangedListener(new LocationDisplay.DataSourceStatusChangedListener() {
                @Override
                public void onStatusChanged(LocationDisplay.DataSourceStatusChangedEvent dataSourceStatusChangedEvent) {
                    if (dataSourceStatusChangedEvent.getSource().getLocationDataSource().getError() == null) {
                        //showMessage("Location Display Started=" + dataSourceStatusChangedEvent.isStarted());
                    } else {
                        //showMessage("Something went wrong.");
                    }
                }
            });
            locationDisplay.startAsync();
            locationDisplay.setShowLocation(true);
            locationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    reminder.setName(disName.getText().toString());
                    reminder.setStatus(Reminder.ENABLE);
                    reminder.setType(current_type);

                    if(reminder.getLatitude() != null && reminder.getLongitude() != null) {
                        db.addAlarm(reminder);
                        showMessage("Add new Alarm");
                        startService(new Intent(getApplication(), ReminderService.class));
                        setResult(RESULT_OK, null);
                        finish();
                    }
                    else {
                        showMessage("Select location");
                    }
                }
            });
        }

        mapView.setMap(map);

        mapView.setOnTouchListener(new DefaultMapViewOnTouchListener(this, mapView) {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                Point clickPoint = mapView.screenToLocation(new android.graphics.Point(Math.round(e.getX()), Math.round(e.getY())));
                SpatialReference spacRef = SpatialReference.create(4326);
                Point point = (Point) GeometryEngine.project(clickPoint, spacRef);
                callout.show(locationMark, point);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                reminder.setLatitude(point.getY());
                reminder.setLongitude(point.getX());
                location.setText(determineLocation(point, locationDisplay.getLocation().getPosition()));

                return true;
            }
        });

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(ReminderSettingActivity.this)
                        .title("Alarm Name")
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("alarm name", disName.getText().toString(), new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                disName.setText(input.toString());
                            }
                        }).show();
            }
        });
        type.setOnClickListener(new View.OnClickListener() {

            String myType[] = { "Silence", "Vibrate" };

            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(ReminderSettingActivity.this)
                        .title("Alarm Type")
                        .items(myType)
                        .itemsCallbackSingleChoice(1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                switch(which) {
                                    case 0:
                                        reminder.setType(Reminder.SILENCE);
                                        current_type = Reminder.SILENCE;
                                        disType.setText("Silence");
                                        break;
                                    case 1:
                                        reminder.setType(Reminder.VIBRATE);
                                        current_type = Reminder.VIBRATE;
                                        disType.setText("Vibrate");
                                        break;
                                }

                                return true;
                            }
                        })
                        .positiveText("Select")
                        .show();
            }
        });
        distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(ReminderSettingActivity.this)
                        .title("Notice Distance")
                        .inputType(InputType.TYPE_NUMBER_VARIATION_NORMAL)
                        .input("distance", Integer.toString(reminder.getDistance()), new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                try{
                                    int dis = Integer.valueOf(input.toString());
                                    disDistance.setText(input.toString());
                                    reminder.setDistance(dis);
                                } catch(Exception ex) {
                                    showMessage("Number only");
                                }

                            }
                        }).show();
            }
        });

    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private String determineLocation(Point p1, Point p2) {
        Location location1 = new Location("");
        Location location2 = new Location("");
        location1.setLatitude(p1.getY());
        location1.setLongitude(p1.getX());
        location2.setLatitude(p2.getY());
        location2.setLongitude(p2.getX());
        double distance = location1.distanceTo(location2);
        if(distance / 1000 < 1) {
            return String.format("%.2f", distance) + " เมตร";
        }
        else {
            return String.format("%.2f", distance / 1000) + " กิโลเมตร";
        }
    }

}
